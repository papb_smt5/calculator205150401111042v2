package id.ub.calculator205150401111042;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {

    TextView tv2,tv3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);
        Intent in = getIntent();
        String hasil =in.getStringExtra("hasil");
        String finalinput=in.getStringExtra("finalinput");
        tv2.setText(finalinput);
        tv3.setText(hasil);
    }
}